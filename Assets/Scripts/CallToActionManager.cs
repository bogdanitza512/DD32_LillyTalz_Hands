﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ScreenMgr;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.Events;

/// <summary>
/// 
/// </summary>
public class CallToActionManager : AnimatorScreen {

    #region Nested Types



    #endregion

    #region Fields and Properties

    [SerializeField]
    private Image progressBar;

    [SerializeField]
    private Image progressFill;

    [SerializeField]
    private float holdDuration = 5f;

    [SerializeField]
    private bool isHandDown = false;

    [SerializeField]
    private Tweener callToActionTweener = null;

    [SerializeField]
    private UnityEvent onProgressBarFillComplete;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    { 

    }

    #endregion

    #region Methods

    public void OnCallToActionButton_PointerDown()
    {
        print("[CallToActionManager]: Pointer Down");

        if(!isHandDown)
        {
            isHandDown = true;
            if (callToActionTweener != null
                && (callToActionTweener.IsActive()
                    || callToActionTweener.IsPlaying()))
            {
                callToActionTweener.Kill();
            }

            callToActionTweener =
                progressFill
                    .DOFillAmount(1, holdDuration - holdDuration * progressFill.fillAmount)
                    .SetEase(Ease.InQuad)
                    .OnComplete(onProgressBarFillComplete.Invoke)
                    ;

        }

    }

    public void OnCallToActionButton_PointerUp()
    {
        print("[CallToActionManager]: Pointer Up");

        if (isHandDown)
        {
            isHandDown = false;
            if(callToActionTweener != null
                && (callToActionTweener.IsActive() 
                    || callToActionTweener.IsPlaying()))
            {
                callToActionTweener.Kill();
            }

            callToActionTweener = 
                progressFill
                    .DOFillAmount(0, holdDuration/2 - holdDuration/2 * (1 - progressFill.fillAmount))
                    .SetEase(Ease.InQuad)
                    ;


        }

    }

    #endregion

}
